'use strict';

/**
 * @ngdoc function
 * @name tictactoeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tictactoeApp, game engine
 */

angular.module('tictactoeApp')
  .controller('MainCtrl', function ($scope) {

    // setup
    winningRows = createAllWinningRows();
    twoInRow_O = createAllRowsWithOneMoveLeft_O();
    twoInRow_X = createAllRowsWithOneMoveLeft_X();

    $scope.grid = createEmptyGrid();
    $scope.currentUser = HUMAN;
    $scope.previousHumanMove = "No previous moves...";
    $scope.previousComputerMove = "No previous moves ...";
    $scope.gameOver = false;
    $scope.winner = "";
    $scope.draw = false;

    $scope.lastHumanMove = [-1,-1];

    $scope.clickCell = function(col, row){
      console.log("row: "+  row);
      console.log("col: "+  col);
      if(!$scope.gameOver) {
        $scope.grid[row][col] = X;
        $scope.lastHumanMove = [row, col];
        $scope.previousHumanMove = "col: " + col + " row: " + row;
        $scope.currentUser = HUMAN;
        checkForWinner(HUMAN);
        checkForDraw($scope.grid);
       

        $scope.currentUser = COMPUTER;
        computerMove();
        checkForWinner(COMPUTER);
        checkForDraw($scope.grid);
       

      }
    };

    // all rows NOT EMPTY, could make this even smarter to detect
    // that the game is over...
    function checkForDraw(grid){
      var draw = true;
      var i;
      var j;
      for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
          var data = grid[i][j];
          if(data === DASH){
            draw = false;
          }
        }
      }
      $scope.draw = draw;
      if(draw){
        $scope.gameOver = true;
      }
    }

    function checkForWinner(currentUser){
      if(!$scope.gameOver){
        var rowWin = checkForWinningRow($scope.grid);
        var colWin = checkForWinningColumn($scope.grid);
        var diaWin = checkForWinDiag($scope.grid);
        if(rowWin || colWin || diaWin){
          $scope.gameOver = true;
          if(currentUser === HUMAN){
            $scope.winner = "HUMAN"

          }else{
            $scope.winner = "COMPUTER"

          }
        }
      }
    }

    function computerMove(){
      var nextMove = calculateNextWinningMove($scope.grid);
      if(nextMove != null)
      {
        var row = nextMove[0];
        var col = nextMove[1];

        $scope.grid[row][col] = O;
      }
      else if(nextMove === null)
      {
        var blockingMoveOut = blockingMove($scope.grid);
        if(blockingMoveOut != null){
          var row = blockingMoveOut[0];
          var col = blockingMoveOut[1];
          console.log("BLOCKING MOVE...");
          $scope.grid[row][col] = O;
        }
        else
        {
          var computerBestMove = bestMove($scope.grid, $scope.lastHumanMove);
          if(computerBestMove != null){
            var row = computerBestMove[0];
            var col = computerBestMove[1];
            console.log("BEST MOVE");
            $scope.grid[row][col] = O;
          }
        }
      }

      $scope.previousComputerMove = "col: " + col + " row: " + row;

    }

    $scope.isCellEnabled = function(cell){
      return cell === DASH;
    };

    $scope.showXimage = function(cell){
      return cell === X;
    };

    $scope.showOimage = function(cell){
      return cell === O;
    };

    $scope.showEmptyImage = function(cell){
      return cell === DASH;
    };

    $scope.resetGame = function(){
      $scope.grid = createEmptyGrid();
      $scope.currentUser = HUMAN;
      $scope.previousHumanMove = "No previous moves...";
      $scope.previousComputerMove = "No previous moves ...";
      $scope.gameOver = false;
      $scope.winner = "";
      $scope.draw = false;
    };
  });

var HUMAN = "Human";
var COMPUTER = "Computer";
var X = "x";
var O = "o";
var DASH = "-";
var winningRows;
var twoInRow_O;
var twoInRow_X;

function blockingMove(grid){
  var returnValue = [];
  var i;
  for(i = 0; i < 3; i++) {
    var row = grid[i];
    if(twoInRow_X.has(row.toString())){
      console.log("a row has two X already... calculating the move");
      var colPos;
      var j;
      for(j = 0; j < 3; j++){
        var val = row[j];
        if(val === DASH){
          colPos = j;
          returnValue[0] = i;
          returnValue[1] = colPos;
          return returnValue;
        }
      }
    }
  }

  // if we didn't find rows, then we should check columns
  var k;
  var flippedGrid = gridFlipper(grid);
  for(k = 0; k < 3; k++) {
    var row2 = flippedGrid[k];
    if(twoInRow_X.has(row2.toString())){
      console.log("a row has two O already... calculating the move");
      var colPosB;
      var m;
      for(m = 0; m < 3; m++){
        var val = row2[m];
        if(val === DASH){
          colPosB = m;
          returnValue[0] = colPosB;
          returnValue[1] = k;
          return returnValue;
        }
      }
    }
  }

  // now check Diags.
  var digWestEast  = buildDiagWestToEast(grid);

  var digEastWest = buildDiagEastToWest(grid);

  if(twoInRow_X.has(digWestEast.toString())){
    var diaRow = findDashPos(digWestEast);
    if(diaRow != -1){
      if(diaRow == 0){
        return [0,0];
      }
      else if (diaRow == 1){
        return [1,1];
      }else{
        return [2,2]
      }
    }
  }
  if(twoInRow_X.has(digEastWest.toString())){
    var diaRowEW = findDashPos(digEastWest);
    if(diaRowEW != -1){
      if(diaRowEW == 0){
        return [0,2];
      }
      else if (diaRowEW == 1){
        return [1,1];
      }else{
        return [2,0]
      }
    }
  }
  return null;
}

function calculateNextWinningMove(grid){
   // check for any rows that may have two O
  var returnValue = [];

  var i;
  for(i = 0; i < 3; i++) {
    var row = grid[i];
    if(twoInRow_O.has(row.toString())){
      console.log("a row has two O already... calculating the move");
      var colPos;
      var j;
      for(j = 0; j < 3; j++){
        var val = row[j];
        if(val === DASH){
          colPos = j;
          returnValue[0] = i;
          returnValue[1] = colPos;
          return returnValue;
        }
      }
    }
  }

    // if we didn't find rows, then we should check columns
    var k;
    var flippedGrid = gridFlipper(grid);
    for(k = 0; k < 3; k++) {
      var row2 = flippedGrid[k];
      if(twoInRow_O.has(row2.toString())){
        console.log("a row has two O already... calculating the move");
        var colPosB;
        var m;
        for(m = 0; m < 3; m++){
          var val = row2[m];
          if(val === DASH){
            colPosB = m;
            returnValue[0] = colPosB;
            returnValue[1] = k;
            return returnValue;
          }
        }
      }
  }

  // now check Diags.

  var digWestEast  = buildDiagWestToEast(grid);

  var digEastWest = buildDiagEastToWest(grid);

  if(twoInRow_O.has(digWestEast.toString())){
    var diaRow = findDashPos(digWestEast);
    if(diaRow != -1){
      if(diaRow == 0){
        return [0,0];
      }
      else if (diaRow == 1){
        return [1,1];
      }else{
        return [2,2]
      }
    }
  }
  if(twoInRow_O.has(digEastWest.toString())){
    var diaRowEW = findDashPos(digEastWest);
    if(diaRowEW != -1){
      if(diaRowEW == 0){
        return [0,2];
      }
      else if (diaRowEW == 1){
        return [1,1];
      }else{
        return [2,0]
      }
    }
  }
  return null;
}

function findDashPos(row){

  var j;
  for(j = 0; j < 3; j++){
    var val = row[j];
    if(val === DASH){
      return j;
    }
  }
  return -1;
}

function bestMove(grid, humanLastMove){

  // if middle is still empty, choose that.
  if(grid[1][1] === DASH){
    return [1,1];
  }
  if(didHumanPutInCorner(humanLastMove.toString()) && grid[1][1] === DASH){
    return [1,1];
  }
  var position = didHumanPutOnEdge(humanLastMove.toString(), grid);
  if(position.length > 0){
    console.log("USER PUT ON EDGE");
    return position;
  }

  var cornerPosition = didHumanPutInCenter(humanLastMove.toString(), grid);
  if(cornerPosition.length > 0){
    console.log("USER PUT IN CENTER");
    return cornerPosition;
  }
  var returnValue = [];
  var i;
  var j;
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      var val = grid[i][j];
      if(val === DASH){
        returnValue[0] = i;
        returnValue[1] = j;
        return returnValue;
      }
    }
  }
  return null;
}

function didHumanPutInCorner(lastMove){
  console.log("last move: ", lastMove);

  var topLeft = [0, 0].toString();
  var topRight = [0, 2].toString();
  var bottomLeft = [2, 0].toString();
  var bottomRight = [2,2].toString();

  return lastMove === topLeft || lastMove === topRight || lastMove === bottomLeft || lastMove === bottomRight;
}

function didHumanPutInCenter(lastMove, grid){

  var center = [1,1].toString();
  if(center === lastMove){
    if(grid[0][0] === DASH){
      return [0,0];
    }
    else if(grid[2][2] === DASH){
      return [2,2];
    }
    else if(grid[0][2] === DASH){
      return [0,2];
    }
    else if(grid[2][0] === DASH){
      return [2,0];
    }
  }
  return [];

}

function didHumanPutOnEdge(lastMove, grid){
  var topEdge = [0,1].toString();
  var bottomEdge = [2,1].toString();
  var leftEdge = [1,0].toString();
  var rightEdge = [1,2].toString();

  if(topEdge === lastMove){
    if(grid[0][0] === DASH){
      return [0,0]
    }
    else if(grid[0][2] === DASH){
      return [0,2];
    }
    else{
      return [];
    }
  }
  if(bottomEdge === lastMove){
    if(grid[2][0] === DASH){
      return [2,0];
    }else if(grid[2][2] === DASH){
    }
    else {
      return [];
    }
  }
  if(leftEdge === lastMove) {
    if (grid[0][0] === DASH) {
      return [0, 0];
    }
    else if (grid[2][0] === DASH) {
      return [2, 0];
    }
    else {
      return [];
    }
  }
    if(rightEdge === lastMove){
      if(grid[0][2] === DASH){
        return [0,2];
      }
      else if(grid[2][2] === DASH){
        return [2,2];
      }
      else{
        return [];
      }
    }
  return [];

}

function createEmptyGrid(){
  console.log("creating grid...");
  var arr = [];
  arr.push([]);
  arr.push([]);
  arr.push([]);
  var i;
  var j;
   for(i = 0; i < 3; i++){
     for(j = 0; j < 3; j++){
       arr[i][j] = DASH;
     }
   }
  console.log(arr);
  return arr;
}

function checkForWinningRow(grid){
  var i;
  for(i = 0; i < 3; i++){
    var row = grid[i];
    console.log("row check: " + row);
    if(winningRows.has(row.toString())){
      console.log("ROW WINNER!");
      return true;
    }
  }
  return false;
}

function checkForWinningColumn(grid){
  var flipRowColGrid = gridFlipper(grid);

  var k;
  for(k = 0; k < 3; k++){
    var col = flipRowColGrid[k];
    if(winningRows.has(col.toString())){
     return true;
    }
  }
  return false;
}

function checkForWinDiag(grid){
  var digWestEast  = buildDiagWestToEast(grid);

  var digEastWest = buildDiagEastToWest(grid);

  if(winningRows.has(digWestEast.toString())){
    return true;
  }
  if(winningRows.has(digEastWest.toString())){
    return true;
  }
  else{
    return false;
  }
}

function buildDiagWestToEast(grid){
  var digWestEast  = [];
  digWestEast[0] = grid[0][0];
  digWestEast[1] = grid[1][1];
  digWestEast[2] = grid[2][2];
  return digWestEast;
}

function buildDiagEastToWest(grid){
  var digEastWest = [];
  digEastWest[0] = grid[0][2];
  digEastWest[1] = grid[1][1];
  digEastWest[2] = grid[2][0];
  return digEastWest;
}

function createAllWinningRows(){

  var winningRows = new Set();

  //row 0, horz array
  var pat1 = [];
  pat1.push([]);
  pat1[0][0] = X;
  pat1[0][1] = X;
  pat1[0][2] = X;

  // row 0, horiz array
  var pat2 = [];
  pat2.push([]);
  pat2[0][0] = O;
  pat2[0][1] = O;
  pat2[0][2] = O;

  winningRows.add(pat1.toString());
  winningRows.add(pat2.toString());

  console.log("Winning Rows: " + winningRows);
  console.log("size: " + winningRows.size);

  return winningRows;

}

function createAllRowsWithOneMoveLeft_O(){
  var pattern1 = [];
  pattern1.push(O);
  pattern1.push(O);
  pattern1.push(DASH);

  var pattern2 = [];
  pattern2.push(O);
  pattern2.push(DASH);
  pattern2.push(O);

  var pattern3 = [];
  pattern3.push(DASH);
  pattern3.push(O);
  pattern3.push(O);

  var rowsWithTwo = new Set();
  rowsWithTwo.add(pattern1.toString());
  rowsWithTwo.add(pattern2.toString());
  rowsWithTwo.add(pattern3.toString());

  return rowsWithTwo;
}

function createAllRowsWithOneMoveLeft_X(){
  var pattern4 = [];
  pattern4.push(X);
  pattern4.push(X);
  pattern4.push(DASH);

  var pattern5 = [];
  pattern5.push(X);
  pattern5.push(DASH);
  pattern5.push(X);

  var pattern6 = [];
  pattern6.push(DASH);
  pattern6.push(X);
  pattern6.push(X);

  var rowsWithTwo = new Set();
  rowsWithTwo.add(pattern4.toString());
  rowsWithTwo.add(pattern5.toString());
  rowsWithTwo.add(pattern6.toString());
  return rowsWithTwo;

}

// helper function to transpose grid
function gridFlipper(grid){
  var flipRowColGrid = [];
  flipRowColGrid.push([]);
  flipRowColGrid.push([]);
  flipRowColGrid.push([]);
  var i;
  var j;
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      var data = grid[i][j]
      flipRowColGrid[j][i] = data;
    }
  }
  return flipRowColGrid;
}

